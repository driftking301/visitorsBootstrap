var app = angular.module('visitorsApp',[]);

app.controller('createController', ['$scope', function ($scope) {
    $scope.isInEditMode = false;
    $scope.isInAddMode=false;
    $scope.isInDelMode=false;

    /*change the login variable to "false" so the
    application can run without restrictions,
    login= "true" must be only enabled for production*/
    $scope.login=true;
    $scope.logout=false

    $scope.index = -1;
    $scope.currentObject = {};

    $scope.visitorObj = {
        name: '',
        company: '',
        innkeeper: '',
        inDate: '',
        outDate:'',
        canOut:true,
    };

    $scope.listOfVisitors = [
        {id: 1, name: 'David Arzate', company: 'Flint', innkeeper: 'Oscar Montero', inDate: "9:30:00", outDate: '11:00', canOut: false},
        {id: 2,name: 'Arturo Dominguez', company: 'ADS', innkeeper: 'Carlos C. Soto', inDate: "10:45", outDate: '12:30', canOut: false},
        {id: 3, name: 'Sara Benitez', company: 'CFE', innkeeper: 'Juan Carlos', inDate: '11:00', outDate: '', canOut: true},
        {id: 4, name: 'Mario Sánchez', company: 'ST', innkeeper: 'Octavio Galvan', inDate: '14:24', outDate: '', canOut: true},
        {id: 5, name: 'Francisco López', company: 'TELMEX', innkeeper: 'Oscar Montero', inDate: '15:29', outDate: '17:54', canOut: false},
        {id: 6, name: 'Emanuel Resendiz', company: 'ENLACE TP', innkeeper: 'Juan Carlos', inDate: '13:14', outDate: '14:21', canOut: false},
        {id: 7, name: 'Luis Martinez', company: 'TOTALPLAY', innkeeper: 'Jorge Rodríguez' , inDate: '11:45', outDate: '13:11', canOut: false},
        {id: 8, name: 'Dario Moreno', company: 'MITUTOYO', innkeeper: 'Ademir Trujillo', inDate: '9:17', outDate: '10:47', canOut: false},
        {id: 9, name: 'Rocío González', company: 'TELCEL', innkeeper: 'Nestor Vargas', inDate: '9:10', outDate: '', canOut: true},
        {id: 10, name: 'Mariana Saavedra', company: 'MOVISTAR', innkeeper: 'Jorge Rodriguez', inDate: '11:54', outDate: '', canOut: true},
        {id: 11, name: 'Dulce Trejo', company: 'DISHER', innkeeper: 'Juan Carlos', inDate: '10:39', outDate: '', canOut: true},
        {id: 12, name: 'Mayra Montufar', company: 'DELL', innkeeper: 'Oscar Montero', inDate: '15:24', outDate: '16:21', canOut: false},
        {id: 13, name: 'Vianey Perez', company: 'ZEBRA', innkeeper: 'Ademir Trujillo', inDate: '17:10', outDate: '17:30', canOut: false},
        {id: 14, name: 'Sunny Lane', company: 'ZUND', innkeeper: 'David Arzate', inDate: '16:45', outDate: '', canOut: true}
    ];

    $scope.innkeepers = [
        {id: 1, name: 'David Arzate'},
        {id: 2, name: 'Carlos C. Soto'},
        {id: 3, name: 'Lesem Barreto'},
        {id: 4, name: 'Ademir Trujillo'},
        {id: 5, name: 'Miriam Herrera'},
        {id: 6, name: 'Nestor Vargas'},
        {id: 7, name: 'Ana Posadas'},
        {id: 8, name: 'Dulce Herrera'},
        {id: 9, name: 'Juan Carlos'},
        {id: 10, name: 'Oscar Montero'},
        {id: 11, name: 'Octavio Galvan'},
        {id: 12, name: 'Jorge Rodríguez'},
        {id: 13, name: 'Azael Enriquez'},
        {id: 14, name: 'María Morales'}
    ];

    $scope.addVisitor = function () {
        $scope.visitorObj.canOut=true;
        $scope.listOfVisitors.push($scope.visitorObj);
        $scope.visitorObj = null;
        $scope.closeDialog();
        $('#myModal').modal('hide');
    };

    $scope.closeDialog = function () {
        $scope.visitorObj = {};
        $scope.isInEditMode = false;
        $scope.isInDelMode=false;
        $scope.isInAddMode=false;
    };

    $scope.editVisitor = function (currVis, index) {
        $scope.isInEditMode = true;
        $scope.visitorObj = angular.copy(currVis);
        $scope.index = index;


        $('#myModal').modal('show');

    };
    $scope.updateVisitor = function() {
        console.log($scope.visitorObj.canOut);
        if($scope.visitorObj.outDate === undefined || $scope.visitorObj.outDate === null)
        {
            $scope.visitorObj.canOut=true;
            console.log($scope.visitorObj.canOut);
        }
        if($scope.visitorObj.outDate != undefined || $scope.visitorObj.outDate != null)
        {
            $scope.visitorObj.canOut=false;
            console.log($scope.visitorObj.canOut);
        }
        $scope.listOfVisitors[$scope.index] = angular.copy($scope.visitorObj);

        $scope.visitorObj= {};
        $scope.closeDialog();
        $('#myModal').modal('hide');
    };

    $scope.visitorOut = function(id){
        for(var i=0,len=$scope.listOfVisitors.length;i<len;i++){
            if($scope.listOfVisitors[i].id === id){
                var date= new Date();
                hour=date.getHours();
                minute=date.getMinutes();

                $scope.listOfVisitors[i].outDate=hour+":"+minute;
                $scope.listOfVisitors[i].canOut=false;

                break;
            }
        }
    };

    $scope.preDelVisitor = function (currVis, index) {
        $scope.isInEditMode= false;
        $scope.isInDelMode = true;

        $scope.visitorObj = angular.copy(currVis);
        $scope.index = index;
        console.log($scope.visitorObj.innkeeper)

        $('#myModal').modal('show');
    };

    $scope.visitorDel = function(){
        $scope.listOfVisitors[$scope.index] = angular.copy($scope.visitorObj);
        console.log($scope.visitorObj.id);
        id=$scope.visitorObj.id;

        for(var i=0,len=$scope.listOfVisitors.length;i<len;i++){
            if($scope.listOfVisitors[i].id === id){
                $scope.listOfVisitors.splice(i,1);
                break;
            }
        }
        $scope.visitorObj= {};
        $scope.closeDialog();
        $('#myModal').modal('hide');
    };

    $scope.preAdd = function(){
      $scope.isInAddMode=true;
    };

    $scope.loginOk = function(){
        $scope.login=false;
        console.log($scope.login);
    };

    $scope.logOutOk = function(){
        $scope.login=true;
        $scope.logout=false;
    };

    $scope.orderBy=function(order){
      $scope.selectOrder= order;
    };

    app.directive('mi-encabezado', function(){
        return{
            template:'<div class="jumbotron"><h1>Registro de Visitantes</h1></div>'
        }
    })
}]);

